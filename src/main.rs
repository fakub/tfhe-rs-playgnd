use tfhe::core_crypto::entities::GlweCiphertext;
use tfhe::shortint::ciphertext::Degree;
use tfhe::shortint::parameters::*;
use tfhe::shortint::prelude::*;
use tfhe::shortint::server_key::LookupTableOwned;

use rayon::prelude::*;

fn gen_no_padding_acc<F>(server_key: &ServerKey, f: F) -> LookupTableOwned
where
    F: Fn(u64) -> u64,
{
    let mut accumulator = GlweCiphertext::new(
        0u64,
        server_key.bootstrapping_key.glwe_size(),
        server_key.bootstrapping_key.polynomial_size(),
        server_key.key_switching_key.ciphertext_modulus(),
    );

    let mut accumulator_view = accumulator.as_mut_view();

    accumulator_view.get_mut_mask().as_mut().fill(0);

    // Modulus of the msg contained in the msg bits and operations buffer
    // Modulus_sup is divided by two as in parmesan
    let modulus_sup = server_key.message_modulus.0 * server_key.carry_modulus.0 / 2;

    // N/(p/2) = size of each block
    let box_size = server_key.bootstrapping_key.polynomial_size().0 / modulus_sup;

    // Value of the shift we multiply our messages by
    // First main change delta is re multiplied by 2 to account for the padding bit
    let delta =
        ((1u64 << 63) / (server_key.message_modulus.0 * server_key.carry_modulus.0) as u64) * 2;

    let mut body = accumulator_view.get_mut_body();
    let accumulator_u64 = body.as_mut();

    // Tracking the max value of the function to define the degree later
    let mut max_value = 0;

    for i in 0..modulus_sup {
        let index = i * box_size;
        accumulator_u64[index..index + box_size]
            .iter_mut()
            .for_each(|a| {
                let f_eval = f(i as u64);
                *a = f_eval * delta;
                max_value = max_value.max(f_eval);
            });
    }

    let half_box_size = box_size / 2;

    // Negate the first half_box_size coefficients
    for a_i in accumulator_u64[0..half_box_size].iter_mut() {
        *a_i = (*a_i).wrapping_neg();
    }

    // Rotate the accumulator
    accumulator_u64.rotate_left(half_box_size);

    LookupTableOwned {
        acc: accumulator,
        degree: Degree(max_value as usize),
    }
}

fn main() {
    println!("\n>>> DEMO PBS with TFHE-rs, function:   x -> x + 4\n");

    // custom params as shortint ignores the bit of padding when defining the message space in the parameter name
    let custom_params = Parameters {
        message_modulus: MessageModulus(1 << 5),
        ..PARAM_MESSAGE_4_CARRY_0
    };

    // generate keys for 5-bit msg space
    let (client_key, server_key) = gen_keys(custom_params);


    // create test vec from func (nice to have: generate_accumulator that inputs a LUT as a vector)
    // =========================================================================
    //  ISSUE: this appears to work as (x) -> (2x) before input ... (y) -> (y/2) after output
    //  e.g., for x + 4, this computes (2x + 4) / 2 = x + 2, which can be indeed observed from the output
    // =========================================================================
    let func = |x: u64| x + 4;
    let acc = gen_no_padding_acc(&server_key, func);


    // -------------------------------------------------------------------------
    // full loop with one extra value to spot the overlap
    for i in 0..=32 {
        // encrypt into the full msg domain
        let c1 = client_key.encrypt_without_padding(i);

        // eval func
        let cf1 = server_key.apply_lookup_table(&c1, &acc);

        // decrypt (include the padding bit) & print
        let m1 = client_key.decrypt_without_padding(&c1);
        let mf1 = client_key.decrypt_without_padding(&cf1);
        println!("(i = {:2}) decrypted IN -> OUT: {:2} -> {:2}", i, m1, mf1);
    }


    // -------------------------------------------------------------------------
    // the same thing in parallel
    println!("\n\n>>> Same thing in parallel\n");
    let mut c_in:  Vec<_> = vec![];
    let mut c_out: Vec<_> = vec![];
    for i in 0..=32 {
        // encrypt into the full msg domain
        c_in.push(client_key.encrypt_without_padding(i));
        // well, this is a bit useless
        c_out.push(client_key.encrypt_without_padding(i));
    }

    c_out.par_iter_mut().zip(c_in.par_iter()).for_each(|(co,ci)| {
        // eval func
        *co = server_key.apply_lookup_table(&ci, &acc);
    });
    c_out.iter().zip(c_in.iter().enumerate()).for_each(|(co,(i,ci))| {
        // decrypt (include the padding bit) & print
        let mi = client_key.decrypt_without_padding(&ci);
        let mfi = client_key.decrypt_without_padding(&co);
        println!("(i = {:2}) decrypted IN -> OUT: {:2} -> {:2}", i, mi, mfi);
    });
}
